#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <iomanip>
#include <math.h>

#include <boost/multiprecision/cpp_dec_float.hpp>   //for big float numbers

/* function for processing input file with data
 * output:      stdout
 * usage:       processInputFile(path, lines)
 * path:        [std::string]                       full path to file with filename
 * lines:       [std::vector<unsigned long int>]    lines to show calculated values on
 * returns:     [bool]                              success or fail */
bool processInputFile(const std::string& path, const std::vector<unsigned long int>& lines)
{
    //open input file
    std::ifstream inputFile(path);
    if (inputFile.is_open()) {
        std::cout << "File '" << path << "' opened" << std::endl;

        //line counter
        int count = 0;

        //big float numbers
        boost::multiprecision::cpp_dec_float_100 number = 0., sumSMA = 0., sumSTD_DEV = 0.;

        //read numbers from file
        for(inputFile >> number; !inputFile.eof(); inputFile >> number) {
            //calculate summ for all numbers from the beginning to end
            sumSMA += number;

            //calculate summ of squares for all numbers
            sumSTD_DEV += number * number;

            //counter tick on line read
            count += 1;

            //output
            if(std::find(lines.begin(),lines.end(),count) != lines.end()) {
                //calculate SMA and declare STD_DEV
                boost::multiprecision::cpp_dec_float_100 SMA = sumSMA / count, STD_DEV = 0.;

                //if current line is the first line, then STD_DEV should be 0
                if (count > 1) {
                    STD_DEV = sqrt((sumSTD_DEV - ((sumSMA * sumSMA) / count)) / (count - 1));
                }

                std::cout << std::defaultfloat << "Line:\t" << count << "\n"
                          << std::setprecision(10) << std::fixed
                          << "\t   Data: " << number << "\n"
                          << "\t    SMA: " << SMA << "\n"
                          << "\tSTD_DEV: " << STD_DEV
                          << std::endl;
            }

            if(count % 1000000 == 0) {
                std::cout << "Line: " << count << "\r";
            }
        }
        std::cout << "Total numbers: " << count << std::endl;

        //close file
        inputFile.close();
        return true;
    }
    else {
        std::cout << "File not opened" << std::endl;
        return false;
    }
}

int main()
{
    //read full file path
    std::string path;
    std::cout << "Enter path to data file: ";
    std::cin >> path;

    //lines to perform calculations
    std::vector<unsigned long int> lines = {1,100,1000000,500000000};

    //process data
    processInputFile(path, lines);
    return 0;
}
