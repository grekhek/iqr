TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -O3

INCLUDEPATH += $$PWD\..\..\externals\vcpkg\installed\x86-windows\include

SOURCES += \
        main.cpp
